import os
import sys
import time
import math
import random
import re

from PyQt5 import QtWidgets, QtCore, QtGui, uic
from PyQt5.QtWidgets import QFileDialog, QTextBrowser, QTableWidgetItem, QMessageBox, QAction
from openpyxl.utils import get_column_letter
from openpyxl.styles import Font
import openpyxl

from algo import solve

scaleValue = 5  # Scale of graphic representation. Should be at least 5

if __name__ != '__main__':
    print(__name__)
    quit()


class UI(QtWidgets.QMainWindow):
    def __init__(self):
        super(UI, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi('ui/main_window.ui', self)  # Load the .ui file
        self.setWindowIcon(QtGui.QIcon('ui/assets/logo.png'))

        self.MainWindow = self.findChild(QtWidgets.QMainWindow, 'MainWindow')

        self.calculateButton = self.findChild(QtWidgets.QPushButton, 'calculateButton')
        self.calculateButton.clicked.connect(self.calculateButton_pressed)

        self.textBox = self.findChild(QTextBrowser, 'textBrowser')

        self.towerTable = self.findChild(QtWidgets.QTableWidget, 'towersTable')
        self.towerTable.setRowCount(12)
        self.towerTable.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Defines actions - buttons from 'Table functions'
        actionClearTable = self.findChild(QAction, 'actionClear_table_2')
        actionClearTable.triggered.connect(self.clearTowerButton_pressed)

        actionEditTable = self.findChild(QAction, 'actionEdit_table')
        actionEditTable.triggered.connect(self.editTableButton_pressed)

        actionImportTable = self.findChild(QAction, 'actionImport_table')
        actionImportTable.triggered.connect(self.importTableButton_pressed)

        actionRandomizeTable = self.findChild(QAction, 'actionRandomize_table')
        actionRandomizeTable.triggered.connect(self.fill_table_with_random_values)

        actionExportTable = self.findChild(QAction, 'actionExport_table')
        actionExportTable.triggered.connect(self.exportTableButton_pressed)

        self.scene = QtWidgets.QGraphicsScene()
        self.graphicsView.setScene(self.scene)

        self.fill_table_with_random_values()

        self.show()

    def drawCircle(self, x, y, rad):
        rect = QtCore.QRectF(
            (float(x) - float(rad)) * scaleValue,
            (float(y) - float(rad)) * scaleValue,
            float(rad) * 2 * scaleValue,
            float(rad) * 2 * scaleValue,
        )
        self.scene.addEllipse(rect)

    def fill_table_with_random_values(self):
        for i in range(self.towerTable.rowCount()):
            for j in range(3):
                self.towerTable.setItem(i, j, (QTableWidgetItem(str(int(random.random() * 100 * 100000) / 100000))))
        self.maxPower = random.random() * 1000
        self.maxPowerLabel.setText('Total power: ' + str(self.maxPower) + ' kWh')

        self.SAMPLES = []
        random_constan = 2
        for j in range(self.towerTable.rowCount()):
            x = [i for i in range(10)]
            y = [(obj ** 1 / random_constan + random.random() * 2 - 1) for obj in x]
            temp = []
            for r in range(len(x)):
                temp.append((x[r], y[r]))
            self.SAMPLES.append(temp)

            formatedSamples = ''
            for p in range(len(x)):
                formatedSamples += str(x[p]) + ',' + str(y[p]) + '; '

            self.towerTable.setItem(j, 3, QTableWidgetItem(str(formatedSamples)))
            random_constan += 1
            if (random_constan > 4):
                random_constan = 2

        self.textBox.setText('Answer will be here')
        self.answer = 0

    def calculateButton_pressed(self):
        self.textBox.setText('Calculations in progress')
        self.scene.clear()

        for i in range(self.towerTable.rowCount()):
            # Table input mistakes parsing
            # ^[0-9]+(\.[0-9]+)?$ float regex
            for j in range(3):
                reg = re.match('^[0-9]+(\.[0-9]+)?$', self.towerTable.item(i, j).text())
                if reg is None:
                    parseErrorWarning = QMessageBox(
                        0,
                        'Warning',
                        'Incorrect value in cell ' + str(i + 1) + ', ' + str(j + 1),
                    )
                    pew = parseErrorWarning.exec_()
                    return
            # Regex for float,float;
            reg = re.match(
                '^(-?[0-9]+(\.[0-9]+)?,-?[0-9]+(\.[0-9]+)?;)+',
                self.towerTable.item(i, 3).text(),
            )
            if reg is None:
                parseErrorWarning = QMessageBox(
                    0,
                    'Warning',
                    'Incorrect value in cell ' + str(i + 1) + ', ' + str(4),
                )
                pew = parseErrorWarning.exec_()
                return

        # Parse coordinates from table to floats
        coordsList = []
        for i in range(self.towerTable.rowCount()):
            coordsTuple = (float(self.towerTable.item(i, 0).text()), float(self.towerTable.item(i, 1).text()))
            coordsList.append(coordsTuple)

        # Parse max energy for each tower from table to floats
        upperBoundsList = [
            float(self.towerTable.item(i, 2).text())
            for i in range(self.towerTable.rowCount())
        ]

        self.SAMPLES = []

        for i in range(self.towerTable.rowCount()):
            sit = ''
            xfull = 0
            strg = self.towerTable.item(i, 3).text()
            temp = []

            for j in range(len(self.towerTable.item(i, 3).text())):
                if strg[j] == ',':
                    xfull = float(sit)
                    sit = ''
                if strg[j] == ';':
                    yfull = float(sit)
                    sit = ''
                    temp.append((xfull, yfull))
                    continue
                if strg[j] != ',' and strg[j] != ';':
                    sit += strg[j]
            self.SAMPLES.append(temp)

        samplesList = self.SAMPLES

        self.textBox.setStyleSheet('''QTextBrowser{color:green}''')

        # Running solve function
        startTime = time.time()
        calculations_result = solve(coordsList, self.maxPower, upperBoundsList, samplesList)
        endTime = time.time()

        self.textBox.setText(
            'Total square of coverage: ' + str(int(calculations_result[2] * 1000) / 1000) + ' m^2 \n' +
            'Calculations done in ' + str(int((endTime-startTime) * 1000) / 1000) + ' seconds.'
        )
        self.answer = int(calculations_result[2] * 1000) / 1000

        # Filling the table with results of calculations
        powers_array = calculations_result[0]
        for i in range(self.towerTable.rowCount()):
            self.towerTable.setItem(i, 4, QTableWidgetItem(str(int(calculations_result[0][i] * 1000) / 1000)))
            self.towerTable.setItem(i, 5, QTableWidgetItem(str(int(calculations_result[1][i] * 1000) / 1000)))
            self.towerTable.setItem(i, 6, QTableWidgetItem(str(int(pow(calculations_result[1][i], 2) * math.pi * 1000) / 1000)))
        self.towerTable.setStyleSheet('''QTableWidget{color:green}''')

        # Creating graphic representation of answer
        for i in range(self.towerTable.rowCount()):
            UI.drawCircle(
                self,
                self.towerTable.item(i, 0).text(),
                self.towerTable.item(i, 1).text(),
                self.towerTable.item(i, 5).text(),
            )
            self.scene.addText(str(i + 1)).setPos(
                (float(self.towerTable.item(i, 0).text()) - 1.5) * scaleValue,
                (float(self.towerTable.item(i, 1).text()) - 2) * scaleValue,
            )

    def clearTowerButton_pressed(self):
        # Clears main table and wipes results
        clearTableWarningWindow = QMessageBox()
        clearTableWarningWindow.setWindowTitle('Table clearing')
        clearTableWarningWindow.setText('Are you sure want to clear the table? Press OK to continue')
        clearTableWarningWindow.setIcon(QMessageBox.Warning)
        clearTableWarningWindow.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        clearTableWarningWindow.setDefaultButton(QMessageBox.Cancel)
        x = clearTableWarningWindow.exec_()

        if x == QMessageBox.Ok:
            self.textBox.setStyleSheet('''QTextBrowser{color:black}''')
            self.textBox.setText('Answer will be here')
            self.towerTable.setStyleSheet('''QTableWidget{color:black}''')
            self.maxPower = 0
            self.maxPowerLabel.setText('Total power: ' + str(self.maxPower) + ' kWh')
            self.scene.clear()

            self.towerTable.setRowCount(1)
            for i in range(7):
                self.towerTable.setItem(0, i, QTableWidgetItem(''))
            return 1

        else:
            return 0

    def editTableButton_pressed(self):
        win2.towersEditTable.setColumnWidth(3, 500)

        win2.show()

        # Changing number of rows by changing value of spinBox
        win2.towerCountSpinBox.valueChanged.connect(self.spinBoxValueChanged)

        win2.maxPowerEdit.setInputMask('9000000')

        win2.clearButton.clicked.connect(self.clearButton_clicked)
        win2.saveButton.clicked.connect(self.saveTableButton_clicked)

        win2.towerCountSpinBox.setValue( self.towerTable.rowCount())
        win2.towersEditTable.setRowCount(self.towerTable.rowCount())
        for i in range(win2.towersEditTable.rowCount()):
            for j in range(4):
                win2.towersEditTable.setItem(i, j, QTableWidgetItem(self.towerTable.item(i, j).text()))

    def importTableButton_pressed(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.AnyFile)
        file_name, _ = dialog.getOpenFileName(
            self,
            'Open file',
            'c:\\',
            'Excel files (*.xls *.xlsx)',
        )
        if not file_name:
            return

        try:
            wb = openpyxl.load_workbook(filename = file_name)
            wr = wb.active

            self.towerTable.setRowCount(int(wr['A1'].value))
            self.maxPower = float(wr['B1'].value)
            self.maxPowerLabel.setText('Total power: ' + str(self.maxPower) + ' kWh')
            for row in range(2, int(wr['A1'].value) + 2):
                for col in range(1, 5):
                    char = get_column_letter(col)
                    self.towerTable.setItem(row - 2, col - 1, QTableWidgetItem(wr[str(char + str(row))].value))
            return
        except:
            QMessageBox(
                0,
                'Warning',
                'The selected file is of the wrong format.',
            ).exec_()


    def exportTableButton_pressed(self):
        if self.towerTable.item(0, 4) is None:
            return
        file_name, _ = QFileDialog.getSaveFileName(
            self,
            'QFileDialog.getSaveFileName()',
            'result',
            'Excel files (*.xlsx *.xls);;All Files (*)',
        )
        if not file_name:
            return

        book = openpyxl.Workbook()
        file = book.active
        file.append([
            str(self.towerTable.rowCount()),
            str(self.maxPower),
            str(self.answer),
        ])
        # Total square of coverage will be green in Excel table, as it is an answer for the problem
        ft = Font(color = '00FF00')
        file['C1'].font = ft
        for i in range(self.towerTable.rowCount()):
            file.append([
                self.towerTable.item(i, j).text()
                for j in range(0, 7)
            ])

        file.append(['X', 'Y', 'Max power', 'Dependence function poinst', 'Power', 'Coverage radius', 'Coverage square'])
        book.save(file_name)

    def spinBoxValueChanged(self):
        win2.towersEditTable.setRowCount(win2.towerCountSpinBox.value())

    def saveTableButton_clicked(self):
        if win2.maxPowerEdit.text() == '':
            powerEditWarning = QMessageBox(
                0,
                'Warning',
                'Max power input must be a valid integer',
            )
            r = powerEditWarning.exec_()
            return

        self.textBox.setStyleSheet('''QTextBrowser{color:black}''')
        self.towerTable.setStyleSheet('''QTableWidget{color:black}''')
        self.textBox.setText('Answer will be here')
        self.scene.clear()

        replaceTableWarning = QMessageBox(0, 'Warning', 'Main table will be replaced by this.\n Press OK to continue')
        replaceTableWarning.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        replaceTableWarning.setDefaultButton(QMessageBox.Cancel)
        replaceTableWarning.setIcon(QMessageBox.Warning)
        w = replaceTableWarning.exec_()

        if w != QMessageBox.Ok:
            return

        self.towerTable.setRowCount(1)
        for i in range(7):
            self.towerTable.setItem(0, i, QTableWidgetItem(''))

        self.towerTable.setRowCount(win2.towersEditTable.rowCount())

        for i in range(win2.towersEditTable.rowCount()):
            for j in range(4):
                self.towerTable.setItem(i, j, QTableWidgetItem(win2.towersEditTable.item(i, j).text()))

        self.maxPower = float(win2.maxPowerEdit.text())
        self.maxPowerLabel.setText('Total power: ' + str(self.maxPower) + ' kWh')

    def clearButton_clicked(self):
        # Clears edit table

        clearTableWarningWindow = QMessageBox()
        clearTableWarningWindow.setWindowTitle('Table clearing')
        clearTableWarningWindow.setText('Are you sure want to clear the table? Press OK to continue')
        clearTableWarningWindow.setIcon(QMessageBox.Warning)
        clearTableWarningWindow.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        clearTableWarningWindow.setDefaultButton(QMessageBox.Cancel)
        x = clearTableWarningWindow.exec_()

        if x != QMessageBox.Ok:
            return

        win2.towersEditTable.setRowCount(1)
        for i in range(3):
            win2.towersEditTable.setItem(0, i, QTableWidgetItem(''))

app = QtWidgets.QApplication(sys.argv)

window = UI()
win2 = uic.loadUi('ui/edit_window.ui')

app.exec_()


'''
             ⣠⣤⣤⣤⣤⣤⣶⣦⣤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⡿⠛⠉⠙⠛⠛⠛⠛⠻⢿⣿⣷⣤⡀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣼⣿⠋⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⠈⢻⣿⣿⡄⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣸⣿⡏⠀⠀⠀⣠⣶⣾⣿⣿⣿⠿⠿⠿⢿⣿⣿⣿⣄⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣿⣿⠁⠀⠀⢰⣿⣿⣯⠁⠀⠀⠀⠀⠀⠀⠀⠈⠙⢿⣷⡄⠀
⠀⠀⣀⣤⣴⣶⣶⣿⡟⠀⠀⠀⢸⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣷⠀
⠀⢰⣿⡟⠋⠉⣹⣿⡇⠀⠀⠀⠘⣿⣿⣿⣿⣷⣦⣤⣤⣤⣶⣶⣶⣶⣿⣿⣿⠀
⠀⢸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠃⠀
⠀⣸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠉⠻⠿⣿⣿⣿⣿⡿⠿⠿⠛⢻⣿⡇⠀⠀
⠀⣿⣿⠁⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣧⠀⠀
⠀⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀
⠀⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀
⠀⢿⣿⡆⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⡇⠀⠀
⠀⠸⣿⣧⡀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⠃⠀⠀
⠀⠀⠛⢿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⣰⣿⣿⣷⣶⣶⣶⣶⠶⠀⢠⣿⣿⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⣽⣿⡏⠁⠀⠀⢸⣿⡇⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⢹⣿⡆⠀⠀⠀⣸⣿⠇⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⢿⣿⣦⣄⣀⣠⣴⣿⣿⠁⠀⠈⠻⣿⣿⣿⣿⡿⠏⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠈⠛⠻⠿⠿⠿⠿⠋⠁⠀⠀⠀⠀⠀⠀
'''