from algo.circle_geometry import (Circle, union_area)


def inverted_target_function(powers: list, *constant_parameters) -> float:
    max_power, positions, power_to_radius, coefs = constant_parameters

    if sum(powers) > max_power:
        return 0

    radii = [
        max(0, power_to_radius[i](power, coefs[i]))
        for i, power in enumerate(powers)
    ]

    circles = [
        Circle(positions[i][0], positions[i][1], radii[i])
        for i in range(len(positions))
    ]
    return -union_area(circles)
