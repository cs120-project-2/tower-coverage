import scipy.optimize as sciop
import numpy as np


def extremum(function, args, bounds: list):
    '''
        function - function to minimize
        args - tuple of constant args. [0] is total power, [1] is a list of tower_positions
        [2] is a list of pow_to_rad functions, [3] is a list of pow_to_rad functions coefficients
        bounds - list of upper power bounds for towers

        Returns extremum coordinates and max function result
    '''
    power_limit_by_bounds = sum(bounds)
    initial_vector = [
        bound / max((power_limit_by_bounds / args[0]), 1) * 0.5
        for bound in bounds
    ]

    bounds = np.array([(0, bound) for bound in bounds], dtype = object)
    results = sciop.differential_evolution(
        function,
        bounds,
        args = args,
        updating = 'deferred',
        x0 = initial_vector,
    )
    return results.x, results.fun
