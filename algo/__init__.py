import numpy as np

from algo.sample_approximation import approximation_from_samples
from algo.target_function import inverted_target_function
from algo.extremum import extremum


def solve(tower_poses: list, total_pow: float, pow_bounds: list, samples: list):
    '''
        tower_poses - list of positions of towers(each position is a tuple (x, y))
        total_power - total power that can be consumed
        pow_bounds - upper bounds of supplied power for every tower
        samples - a list of samples for each tower. Each sample is itself a list of points.
        Point is a tuple (x, y)

        Returns powers where extremum is achieved, radii, and area
    '''
    functions = []
    coefs = []
    for sample in samples:
        x = np.array([point[0] for point in sample], dtype=float)
        y = np.array([point[1] for point in sample], dtype=float)

        res = approximation_from_samples(x, y)
        functions.append(res[0])
        coefs.append(res[1])

    args = (total_pow, tower_poses, functions, coefs)
    powers, min_res = extremum(inverted_target_function, args, pow_bounds)
    radii = [
        max(0, functions[i](power, coefs[i]))
        for i, power in enumerate(powers)
    ]

    return powers, radii, -min_res
