import math

class Circle:
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.r == other.y

    def __repr__(self):
        return f'Circle({self.x}, {self.y}, {self.r})'


def intersection(c1: Circle, c2: Circle) -> bool:
    d = math.sqrt((c2.x - c1.x) ** 2 + (c2.y - c1.y) ** 2)
    return c1.r + c2.r >= d > abs(c1.r - c2.r)


def intersection_points(c1: Circle, c2: Circle) -> [int]:
    d = math.sqrt((c2.x - c1.x) ** 2 + (c2.y - c1.y) ** 2)
    b = (c1.r ** 2 - c2.r ** 2 + d ** 2) / (2 * d)
    x_mid = c1.x + b * (c2.x - c1.x) / d
    y_mid = c1.y + b * (c2.y - c1.y) / d
    h = math.sqrt(c1.r ** 2 - b ** 2)
    x1, x2 = x_mid + h * (c2.y - c1.y) / d, x_mid - h * (c2.y - c1.y) / d
    y1, y2 = y_mid - h * (c2.x - c1.x) / d, y_mid + h * (c2.x - c1.x) / d
    return [x1, y1, x2, y2]


def gth(x, y) -> float:
    if x > 0.0 and y == 0.0:
        return 0.0
    if x > 0.0 and y > 0.0:
        return math.atan(y / x)
    if x == 0.0 and y > 0:
        return math.pi / 2.0
    if x < 0.0 and y > 0.0:
        return math.pi - math.atan(abs(y / x))
    if x < 0.0 and y == 0.0:
        return math.pi
    if x < 0.0 and y < 0.0:
        return math.pi + math.atan(abs(y / x))
    if x == 0.0 and y < 0.0:
        return 1.5 * math.pi
    if x > 0.0 and y < 0.0:
        return 2 * math.pi - math.atan(abs(y / x))
    return 0.0


def parg(d: float) -> float:
    if d >= 2 * math.pi:
        d -= 2 * math.pi
    return d


def calc(x, y, r, theta) -> float:
    return r * (x * math.sin(theta)) + r ** 2 * (theta + math.sin(theta) * math.cos(theta)) * 0.5


def union_area(circles: []) -> float:
    n = len(circles)
    inner = [False] * n
    for i in range(n):
        for j in range(n):
            if i != j and (not inner[i]) and is_inner(circles[i], circles[j]):
                inner[j] = True
    c = [circles[i] for i in range(n) if not inner[i]]
    n = len(c)
    res = 0.0
    for i in range(n):
        iPnts = []
        for j in range(n):
            if i != j and intersection(c[i], c[j]):
                ans = intersection_points(c[i], c[j])
                theta1 = gth(ans[0] - c[i].x, ans[1] - c[i].y)
                theta2 = gth(ans[2] - c[i].x, ans[3] - c[i].y)
                if theta1 > theta2:
                    theta1, theta2 = theta2, theta1

                if (c[j].x - c[i].x - c[i].r * math.cos((theta1 + theta2) / 2.0)) ** 2 + (c[j].y - c[i].y - c[i].r * math.sin((theta1 + theta2)/2.0)) ** 2 < c[j].r ** 2:
                    iPnts.append((theta1, theta2))
                else:
                    if (c[j].x - c[i].x - c[i].r * math.cos(parg((theta1 + theta2) / 2.0 + math.pi))) ** 2 + (c[j].y - c[i].y - c[i].r * math.sin(parg((theta1 + theta2) / 2.0 + math.pi))) ** 2 < c[j].r ** 2:
                        iPnts.append((theta2, 2 * math.pi))
                        iPnts.append((0.0, theta1))
        if len(iPnts) == 0:
            res += math.pi * c[i].r ** 2
        else:
            iPnts.sort()
            theta1, theta2 = iPnts[0][0], iPnts[0][1]
            intlims = [(0.0, theta1)]

            for j in range(len(iPnts)):
                while j < len(iPnts) and theta2 >= iPnts[j][0]:
                    theta2 = max(iPnts[j][1], theta2)
                    j += 1
                if j < len(iPnts):
                    intlims.append((theta2, iPnts[j][0]))
                    theta1, theta2 = iPnts[j][0], iPnts[j][1]
            intlims.append((theta2, 2 * math.pi))

            for j in range(len(intlims)):
                if not (intlims[j][0] == 0.0 and intlims[j][1] == 2 * math.pi) and (intlims[j][0] != intlims[j][1]):
                    res += calc(c[i].x, c[i].y, c[i].r, intlims[j][1]) - calc(c[i].x, c[i].y, c[i].r, intlims[j][0])
    return res


def is_inner(c1: Circle, c2: Circle) -> bool:
    return math.sqrt((c2.x - c1.x) ** 2 + (c2.y - c1.y) ** 2) + c2.r <= c1.r
