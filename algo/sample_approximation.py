import numpy as np
def approximation_from_samples(sample_xs: np.array, sample_ys: np.array):
    candidate_functions = [
        root2_parameterised,
        root3_parameterised,
        root4_parameterised,
    ]
    goodnesses_and_optimal_parameters = [
        approximation_goodness_with_optimal_parameters(f, sample_xs, sample_ys)
        for f in candidate_functions
    ]
    best_goodness = min(g for (g, a) in goodnesses_and_optimal_parameters)
    for f, (g, a) in zip(candidate_functions, goodnesses_and_optimal_parameters):
        if g == best_goodness:
            return (f, a)

def root2_parameterised(x, a):
    return a * np.power(x, 1 / 2)

def root3_parameterised(x, a):
    return a * np.power(x, 1 / 3)

def root4_parameterised(x, a):
    return a * np.power(x, 1 / 4)

def approximation_goodness_with_optimal_parameters(f, sample_xs: np.array, sample_ys: np.array):
    a = optimal_parameters(f, sample_xs, sample_ys)
    approximation_ys = f(sample_xs, a)
    return (approximation_goodness(sample_ys, approximation_ys), a)

def optimal_parameters(f, sample_xs, sample_ys):
    if f == root2_parameterised:
        a = sum(sample_ys * np.power(sample_xs, 1 / 2)) / (sum(np.absolute(sample_xs)))
    if f == root3_parameterised:
        a = sum(sample_ys * np.power(sample_xs, 1 / 3)) / (sum(np.power(sample_xs, 2 / 3)))
    if f == root4_parameterised:
        a = sum(sample_ys * np.power(sample_xs, 1 / 4)) / (sum(np.power(sample_xs, 1 / 2)))
    return (a)

def approximation_goodness(sample_ys: np.array, approximation_ys: np.array):
    return sum(np.power((sample_ys - approximation_ys), 2))
